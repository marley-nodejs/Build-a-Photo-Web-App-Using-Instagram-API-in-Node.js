// Grab all the packages
var express = require('express');
var config = require('./config.js');
var instagram = require('instagram-node').instagram();

var app = express();

app.use(express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

instagram.use({
    client_id: 'bf481134f9074a77acf4f7f45b2ae48c',
    client_secret: 'a4ce35ac14cf4ab9b0ac156404134d09'
});

app.get('/', function(req, res){
    instagram.media_popular(function(err, medias, remaining, limit){
        res.render('pages/index', { grams: medias });
    });
});

app.listen(config.port, function(err){
    if(err){
        console.log("Error");
    } else {
        console.log("Listening on port " + config.port);
    }
});
